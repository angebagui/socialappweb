<?php

namespace App\Utils;

use App\Utils\FirebaseMessagingUtils;
use App\Models\Customer;
use App\Models\CustomerDevice;
use App\Models\Message;

class MessageNotificationUtils{


    public static function notify($customerId, $message){

        $customer = Customer::where(['id' => $customerId])->first();
        if(!empty($customer)){

            $devices = CustomerDevice::where(['customer_id' => $customerId])->get();

            if(!empty($devices)){
               

                foreach($devices as $device){

                    if(!is_null($device->firebase_id)){


                        /** @var Customer $sender*/
                        $sender = $message->getSenderAttribute();

                        if($sender != null){
                          $senderName = $sender->first_name.' '.$sender->last_name;
                        }else{
                            $senderName = ''; 
                        }

                        $title = $senderName.' vous a envoyé un message.';
                        
                        if($message->content_type == 'text'){
                            $body = $message->content;
                        }else{
                            $body = 'Media';
                        }
                        
                        $type = 'chat';

                        $metadata = $message->toArray();

                        $id = $device->firebase_id;

                        FirebaseMessagingUtils::sendNotification($title, $body, $type, $metadata, $id);

                    }

                }


            }
         
        }

        


    }

}