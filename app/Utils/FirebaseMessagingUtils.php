<?php

namespace App\Utils;

class FirebaseMessagingUtils{

    const FCM_TOKEN = "AAAApcQTggc:APA91bG6gfBrM-IRyq_7HJtMrXYvQQF2maV2B3fS5bycNdpwoC_yCmVjShDh1r3MZpMBnjV623AYg3ABfuqL40HXzkphmOJ2GqY6ZJYoP4thF7Eup0ceZXQKEO4MlIZ7WAsZcwwJ94fj";

    public static function sendNotification($title, $body, $type, $metadata, $id) {

        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array (
            'to' => $id,
            'notification' => array (
                "title" => $title,
                "body" => $body,
                "type" => $type

            ),
            'priority' => 'high',
            'data' => array(
                'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
                'id' => '1',
                'status' => 'done',
                'meta_data_id' => "".$metadata["id"],
                'metadata' => json_encode($metadata)
            )
        );

        $fields = json_encode ($fields);

        $headers = array (
            'Authorization: key=' .self::FCM_TOKEN,
            'Content-Type: application/json'
        );

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

        $result = curl_exec ( $ch );
        curl_close ( $ch );

        return $result;
    }

    public static function sendData($metadata, $id) {

        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array (
            'to' => $id,
            'priority' => 'high',
            'data' => array(
                'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
                'id' => '1',
                'status' => 'done',
                'metadata' => json_encode($metadata)
            )
        );
        $fields = json_encode ($fields);

        $headers = array (
            'Authorization: key=' .self::FCM_TOKEN,
            'Content-Type: application/json'
        );

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

        $result = curl_exec ( $ch );
        curl_close ( $ch );

        return $result;
    }
}
