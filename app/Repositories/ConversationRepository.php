<?php

namespace App\Repositories;

use App\Models\Conversation;
use App\Repositories\BaseRepository;

/**
 * Class ConversationRepository
 * @package App\Repositories
 * @version September 7, 2019, 11:46 am UTC
*/

class ConversationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'speakers',
        'is_group',
        'group_name',
        'admins'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Conversation::class;
    }
}
