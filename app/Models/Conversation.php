<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * Class Conversation
 * @package App\Models
 * @version September 7, 2019, 11:46 am UTC
 *
 * @property string speakers
 * @property boolean is_group
 * @property string group_name
 * @property string admins
 */
class Conversation extends Model
{
    use SoftDeletes;

    public $table = 'conversations';


    protected $dates = ['deleted_at'];

    protected  $appends = ['speaker_list', 'users', 'messages'];


    public $fillable = [
        'speakers',
        'is_group',
        'group_name',
        'admins'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'speakers' => 'array',
        'is_group' => 'boolean',
        'group_name' => 'string',
        'admins' => 'array'
    ];



    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getSpeakerListAttribute(){
        $speakerList = Collection::make([]);

        $speakers = $this->speakers;

        if(is_array($speakers)){
            foreach ($speakers as $speaker){
                $speakerList->push((int)$speaker);
            }
        }

        return $speakerList;
    }

    public function getUsersAttribute(){

        $speakerList = Collection::make([]);
        $speakers =  $this->speakers;

        if(is_array($speakers)){
            foreach ($speakers as $speaker){
                $user = Customer::where(['id' => $speaker])->first();
                if($user != null){
                    $speakerList->push($user);
                }

            }
        }

        return $speakerList;
    }

    public function getMessagesAttribute(){

        return Message::where(["conversation_id" => $this->id])->get();

    }

    public function getSpeakersAttribute($value){
        if($value == null){
            return [];
        }
        $json_array =  json_decode(stripslashes($value), true);
        return $json_array ;
    }

    public function getAdminsAttribute($value){
        if($value == null){
            return [];
        }
        $json_array =  json_decode(stripslashes($value), true);
        return $json_array ;
    }


}
