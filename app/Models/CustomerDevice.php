<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CustomerDevice extends Model
{
    use SoftDeletes;

    public $table = 'customer_devices';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'customer_id',
        'firebase_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'firebase_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
