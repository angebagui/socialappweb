<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * Class Message
 * @package App\Models
 * @version September 7, 2019, 11:48 am UTC
 *
 * @property string content
 * @property integer sender_id
 * @property integer conversation_id
 * @property boolean is_read
 * @property boolean is_received
 * @property boolean is_sent
 * @property string content_type
 */
class Message extends Model
{
    use SoftDeletes;

    public $table = 'messages';
    

    protected $dates = ['deleted_at'];

    protected $appends = ['sender'];


    public $fillable = [
        'content',
        'sender_id',
        'conversation_id',
        'is_read',
        'is_received',
        'is_sent',
        'content_type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'content' => 'string',
        'sender_id' => 'integer',
        'conversation_id' => 'integer',
        'is_read' => 'boolean',
        'is_received' => 'boolean',
        'is_sent' => 'boolean',
        'content_type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    public function getSenderAttribute(){
        return Customer::where(['id' => $this->sender_id])->first();
    }



    
}
