<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerAPIRequest;
use App\Http\Requests\API\UpdateCustomerAPIRequest;
use App\Models\Conversation;
use App\Models\Customer;
use App\Repositories\ConversationRepository;
use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Response;

/**
 * Class CustomerController
 * @package App\Http\Controllers\API
 */

class CustomerAPIController extends AppBaseController
{
    /** @var  CustomerRepository */
    private $customerRepository;

    private  $conversationRepository;

    public function __construct(CustomerRepository $customerRepo,  ConversationRepository $conversationRepo)
    {
        $this->customerRepository = $customerRepo;
        $this->conversationRepository = $conversationRepo;
    }

    /**
     * Display a listing of the Customer.
     * GET|HEAD /customers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $customers = $this->customerRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($customers->toArray(), 'Customers retrieved successfully');
    }

    public function findContact($id)
    {
        $customers = Customer::where('id', "!=", $id)->get();

        return $this->sendResponse($customers->toArray(), 'Customers retrieved successfully');
    }

    public function  openConversation(Request $request){

        $speakers = $request->input('speakers');

        $conversations = $this->conversationRepository->all();

        $conversation = null;
        if(!empty($conversations)){
            foreach ($conversations as $conversationItem){

                /** @var Collection $speakerList */
                $speakerList = $conversationItem->getSpeakerListAttribute();
                $speakerIsInList = Collection::make([]);
                if(is_array($speakers)){
                    foreach ($speakers as $speakerId){

                        if($speakerList->contains($speakerId)){
                            $speakerIsInList->push(true);
                        }
                    }
                }

                if($speakerList->count() == $speakerIsInList->count()){
                    $conversation = $conversationItem;
                    break;
                }

            }
        }

        if ($conversation == null) {
            /** @var Conversation $conversation */
            $conversation = $this->conversationRepository->create([
                'speakers' => json_encode($speakers),
                'is_group' => false,
                'group_name' => null,
                'admins' => null
            ]);
        }

        return $this->sendResponse($conversation->toArray(), 'Conversation retrieved successfully');
    }

    public function findByCustomer($customerId){

        /** @var Customer $customer */
        $customer = Customer::where(["id" => $customerId])->first();
        if(empty($customer)){
            return $this->sendError('Customer not found');
        }

        //$conversations = DB::table('conversations')->whereRaw("JSON_CONTAINS(speakers, '[".$customerId."]' )")->get();
        $conversations = Conversation::whereRaw("JSON_CONTAINS(speakers, '[".$customerId."]' )")->get();

        return $this->sendResponse($conversations->toArray(), 'Conversations retrieved successfully');

    }

    /**
     * Store a newly created Customer in storage.
     * POST /customers
     *
     * @param CreateCustomerAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerAPIRequest $request)
    {
        $input = $request->all();

        $customer = $this->customerRepository->create($input);

        return $this->sendResponse($customer->toArray(), 'Customer saved successfully');
    }

    public function signUp(CreateCustomerAPIRequest $request)
    {
        $input = $request->all();
        if(!array_key_exists('email', $input)){
            return $this->sendError('email is required', 400);
        }

        if(!array_key_exists('password', $input)){
            return $this->sendError('password  is required', 400);
        }

        if(!array_key_exists('first_name', $input)){
            return $this->sendError('first_name is required', 400);
        }

        if(!array_key_exists('last_name', $input)){
            return $this->sendError('last_name is required', 400);
        }

        $customer = Customer::where(["email" => $request->email])->first();
        if(!empty($customer)){
            return $this->sendError('Email already exist', 400);
        }


        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];



        $input["password"] = Hash::make($request->password);
        $customer = $this->customerRepository->create($input);

        $token = Auth::attempt($credentials);

        $user = Auth::user();

        return response()->json([
           "data" => [
            'user' => $user,
            'authorization' => [
                'token' => $token,
                'type' => 'bearer',
            ],
        ],
        "message" => "Connected",
        "success" => true
        ]);
    }

    public function login(CreateCustomerAPIRequest $request)
    {


        $input = $request->all();
        if(!array_key_exists('email', $input)){
            return $this->sendError('email is required', 400);
        }

        if(!array_key_exists('password', $input)){
            return $this->sendError('password  is required', 400);
        }

        $credentials = $request->only('email', 'password');
        $token = Auth::attempt($credentials);

        if (!$token) {
            return response()->json([
                'message' => 'Unauthorized',
                "success" => false
            ], 401);
        }

        $user = Auth::user();

        return response()->json([
            "data" => [
             'user' => $user,
             'authorization' => [
                 'token' => $token,
                 'type' => 'bearer',
             ],
            ],
            "message" => "Connected",
            "success" => true
         ]);


    }

    public function logout()
    {
        Auth::logout();
        return response()->json([
            'message' => 'Successfully logged out',
            "success" => true
        ]);
    }

    public function refresh()
    {

        return response()->json([
            "data" => [
             'user' => Auth::user(),
             'authorization' => [
                 'token' => Auth::refresh(),
                 'type' => 'bearer',
             ],
             "message" => "Connected",
             "success" => true
            ]
         ]);
    }

    /**
     * Display the specified Customer.
     * GET|HEAD /customers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Customer $customer */
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            return $this->sendError('Customer not found');
        }

        return $this->sendResponse($customer->toArray(), 'Customer retrieved successfully');
    }

    /**
     * Update the specified Customer in storage.
     * PUT/PATCH /customers/{id}
     *
     * @param UpdateCustomerAPIRequest $request
     *
     * @return Response
     */
    public function update(UpdateCustomerAPIRequest $request)
    {
        $input = $request->all();

        /** @var Customer $customer */
        $customer  = Auth::user();

        if (empty($customer)) {
            return $this->sendError('Customer not found');
        }

        $customer = $this->customerRepository->update($input, $id);

        return $this->sendResponse($customer->toArray(), 'Customer updated successfully');
    }

    /**
     * Remove the specified Customer from storage.
     * DELETE /customers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Customer $customer */
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            return $this->sendError('Customer not found');
        }

        $customer->delete();

        return $this->sendResponse($id, 'Customer deleted successfully');
    }
}
