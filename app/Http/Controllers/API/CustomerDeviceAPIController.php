<?php

namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\AppBaseController;
use App\Models\Customer;
use App\Models\CustomerDevice;

class CustomerDeviceAPIController extends AppBaseController {


    public function store(Request $request){

        $customer = Customer::where(['id' => $request->customer_id])->first();

        if(empty($customer)){

            return $this->sendError('Customer not found');

        }


        $device = CustomerDevice::where(['firebase_id' => $request->firebase_id])->first();

        if(empty($device)){

            $device = CustomerDevice::create($request->all());

            $message = 'Device saved successfully';

        }else{
            if($device->customer_id == $request->customer_id){
                    //Nothing to do

                    $message = 'Device already exists';
            }else{

                $device->customer_id = $request->customer_id;
                $device->save();

                $message = 'Device updated with new customer_id';


            }

        }

        return $this->sendResponse($device->toArray(), $message);


    }
}