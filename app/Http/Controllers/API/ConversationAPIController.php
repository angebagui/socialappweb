<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateConversationAPIRequest;
use App\Http\Requests\API\UpdateConversationAPIRequest;
use App\Models\Conversation;
use App\Models\Customer;
use App\Repositories\ConversationRepository;
use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Collection;
use Response;
use Illuminate\Support\Facades\DB;

/**
 * Class ConversationController
 * @package App\Http\Controllers\API
 */

class ConversationAPIController extends AppBaseController
{
    /** @var  ConversationRepository */
    private $conversationRepository;

    /** @var  CustomerRepository */
    private $customerRepository;

    public function __construct(ConversationRepository $conversationRepo, CustomerRepository $customerRepo)
    {
        $this->conversationRepository = $conversationRepo;
        $this->customerRepository = $customerRepo;
    }

    /**
     * Display a listing of the Conversation.
     * GET|HEAD /conversations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $conversations = $this->conversationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($conversations->toArray(), 'Conversations retrieved successfully');
    }

    public function  openConversation(Request $request){

        $speakers = (array)$request->input('speakers');

        $conversation = Conversation::whereJsonContains('speakers', $speakers)->first();

        if ($conversation == null) {

            $conversation = Conversation::create([
                'speakers' => $speakers,
                'is_group' => boolval ($request->input('is_group', false)),
                'group_name' => $request->input('group_name', null),
                'admins' => $request->input('admins', null) == null ? null: $request->input('admins')
            ]);
             /** @var Conversation $conversation */
            $conversation = Conversation::where('id', $conversation->id)->first();
        }

        return $this->sendResponse($conversation->toArray(), 'Conversation retrieved successfully');
    }


    public function findByCustomer($customerId){

        $customer = Customer::where(["id" => $customerId])->first();
        if(empty($customer)){
            return $this->sendError('Customer not found');
        }


        $conversations = Conversation::whereRaw("JSON_CONTAINS(speakers, '[".$customerId."]' )")->get();

        $conversations_result = collect([]);
        foreach($conversations as $conversation){
            $messages_count =  \App\Models\Message::where(["conversation_id" => $conversation->id])->count();
            if($messages_count>0){
                $conversations_result->push($conversation);
            }
        }

        return $this->sendResponse($conversations_result->toArray(), 'Conversations retrieved successfully');


    }
    public function findMessages($conversation_id){
        /** @var Conversation $conversation */
        $conversation = $this->conversationRepository->find($conversation_id);

        if (empty($conversation)) {
            return $this->sendError('Conversation not found');
        }

        $messages =  \App\Models\Message::where(["conversation_id" => $conversation_id])->get();

        return $this->sendResponse($messages->toArray(), 'Conversation retrieved successfully');
    }

    /**
     * Store a newly created Conversation in storage.
     * POST /conversations
     *
     * @param CreateConversationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateConversationAPIRequest $request)
    {
        $input = $request->all();

        $conversation = $this->conversationRepository->create($input);

        return $this->sendResponse($conversation->toArray(), 'Conversation saved successfully');
    }


    /**
     * Display the specified Conversation.
     * GET|HEAD /conversations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Conversation $conversation */
        $conversation = $this->conversationRepository->find($id);

        if (empty($conversation)) {
            return $this->sendError('Conversation not found');
        }

        return $this->sendResponse($conversation->toArray(), 'Conversation retrieved successfully');
    }

    /**
     * Update the specified Conversation in storage.
     * PUT/PATCH /conversations/{id}
     *
     * @param int $id
     * @param UpdateConversationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateConversationAPIRequest $request)
    {
        $input = $request->all();

        /** @var Conversation $conversation */
        $conversation = $this->conversationRepository->find($id);

        if (empty($conversation)) {
            return $this->sendError('Conversation not found');
        }

        $conversation = $this->conversationRepository->update($input, $id);

        return $this->sendResponse($conversation->toArray(), 'Conversation updated successfully');
    }

    /**
     * Remove the specified Conversation from storage.
     * DELETE /conversations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Conversation $conversation */
        $conversation = $this->conversationRepository->find($id);

        if (empty($conversation)) {
            return $this->sendError('Conversation not found');
        }

        $conversation->delete();

        return $this->sendResponse($id, 'Conversation deleted successfully');
    }
}
