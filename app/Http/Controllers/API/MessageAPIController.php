<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMessageAPIRequest;
use App\Http\Requests\API\UpdateMessageAPIRequest;
use App\Models\Conversation;
use App\Models\Customer;
use App\Models\Message;
use App\Repositories\ConversationRepository;
use App\Repositories\CustomerRepository;
use App\Repositories\MessageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Response;
use App\Utils\MessageNotificationUtils ;

/**
 * Class MessageController
 * @package App\Http\Controllers\API
 */

class MessageAPIController extends AppBaseController
{
    /** @var  MessageRepository */
    private $messageRepository;

    /** @var  ConversationRepository */
    private $conversationRepository;

    /** @var  CustomerRepository */
    private $customerRepository;


    public function __construct(MessageRepository $messageRepo, ConversationRepository $conversationRepo, CustomerRepository $customerRepo)
    {
        $this->messageRepository = $messageRepo;
        $this->conversationRepository = $conversationRepo;
        $this->customerRepository = $customerRepo;
    }

    /**
     * Display a listing of the Message.
     * GET|HEAD /messages
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $messages = $this->messageRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($messages->toArray(), 'Messages retrieved successfully');
    }

    /**
     * Store a newly created Message in storage.
     * POST /messages
     *
     * @param CreateMessageAPIRequest $request
     *
     * @return Response
     */
    public function storeBis(CreateMessageAPIRequest $request)
    {
        $input = $request->all();

        $message = $this->messageRepository->create($input);

        return $this->sendResponse($message->toArray(), 'Message saved successfully');
    }

    /**
     * Store a newly created Message in storage.
     * POST /messages
     *
     * @param CreateMessageAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMessageAPIRequest $request)
    {
        if(!$request->has('content')){
            return $this->sendError('content is required');
        }

        if(!$request->has('content_type')){
            return $this->sendError('content_type is required');
        }

        //if(!$request->json()->has('created_at')){
         //   return $this->sendError('created_at is required');
        //}

        if(!$request->has('sender_id')){
            return $this->sendError('sender_id is required');
        }

        if(!$request->has('conversation_id')){
            return $this->sendError('conversation_id is required');
        }

        //if(!$request->json()->has('client_id')){
          //  return $this->sendError('client_id is required');
        //}

        $input = $request->all();

        //$messages = Message::where(['client_id' => $input['client_id']])->get();

        $messages = [];

        if(count($messages) == 0){

            $conversation = Conversation::where(['id' => $input['conversation_id']])->first();
            if(empty($conversation)){
                return $this->sendError('Conversation not found');
            }

            //$input['created_at'] = Carbon::parse($input['created_at'])->timestamp;
            $input['is_read'] = false;
            $input['is_received'] = false;
            $input['is_sent'] = true;

            $message = $this->messageRepository->create($input);

            //Send Notification to receivers
            /** @var Collection $speakers */
            $speakers = $conversation->getSpeakerListAttribute();

            /** @var Collection $receivers */
            $receivers = Collection::make([]);
            $speakers->each(function ($item)use ($receivers, $input){
                if($item != $input['sender_id']){
                    $receivers->push($item);
                }
            });

            $receivers->each(function ($receiver)use ($message){
                MessageNotificationUtils::notify($receiver, $message);
            });

            return $this->sendResponse($message->toArray(), 'Message saved successfully');

        }else{

            return $this->sendResponse($messages[0], 'Message saved successfully');
        }


    }

    /**
     * Display the specified Message.
     * GET|HEAD /messages/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Message $message */
        $message = $this->messageRepository->find($id);

        if (empty($message)) {
            return $this->sendError('Message not found');
        }

        return $this->sendResponse($message->toArray(), 'Message retrieved successfully');
    }

    /**
     * Update the specified Message in storage.
     * PUT/PATCH /messages/{id}
     *
     * @param int $id
     * @param UpdateMessageAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMessageAPIRequest $request)
    {
        $input = $request->all();

        /** @var Message $message */
        $message = $this->messageRepository->find($id);

        if (empty($message)) {
            return $this->sendError('Message not found');
        }

        $message = $this->messageRepository->update($input, $id);

        return $this->sendResponse($message->toArray(), 'Message updated successfully');
    }

    /**
     * Remove the specified Message from storage.
     * DELETE /messages/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Message $message */
        $message = $this->messageRepository->find($id);

        if (empty($message)) {
            return $this->sendError('Message not found');
        }

        $message->delete();

        return $this->sendResponse($id, 'Message deleted successfully');
    }
}
