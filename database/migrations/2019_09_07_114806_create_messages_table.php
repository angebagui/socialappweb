<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessagesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('content')->nullable();
            $table->bigInteger('sender_id')->nullable();
            $table->bigInteger('conversation_id')->nullable();
            $table->boolean('is_read')->nullable()->default(false);
            $table->boolean('is_received')->nullable()->default(false);
            $table->boolean('is_sent')->nullable()->default(false);
            $table->string('content_type')->nullable()->default("");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
