<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Message;
use Faker\Generator as Faker;

$factory->define(Message::class, function (Faker $faker) {

    return [
        'content' => $faker->text,
        'sender_id' => $faker->randomDigitNotNull,
        'conversation_id' => $faker->word,
        'is_read' => $faker->word,
        'is_received' => $faker->word,
        'is_sent' => $faker->word,
        'content_type' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
