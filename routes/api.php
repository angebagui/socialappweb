<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('customers', 'CustomerAPIController');
Route::get('contacts/{user_id}', 'CustomerAPIController@findContact')->middleware("auth:customers");
Route::post('user_auth', 'CustomerAPIController@login');
Route::post('user_register', 'CustomerAPIController@signUp');
Route::post('refresh_token', 'CustomerAPIController@refresh')->middleware("auth:customers");
Route::post('user_logout', 'CustomerAPIController@logout')->middleware("auth:customers");

Route::put('user_edit', 'CustomerAPIController@update')->middleware("auth:customers");

Route::resource('conversations', 'ConversationAPIController')->middleware("auth:customers");
Route::get('conversations/messages/{conversation_id}', 'ConversationAPIController@findMessages')->middleware("auth:customers");
Route::get('conversations/customers/{customerId}', 'ConversationAPIController@findByCustomer')->middleware("auth:customers");
Route::post('open_conversation', 'ConversationAPIController@openConversation')->middleware("auth:customers");

Route::post('group/create', 'ConversationAPIController@store')->middleware("auth:customers");
Route::post('group/user/add', 'ConversationAPIController@addToGroup')->middleware("auth:customers");
Route::post('group/user/delete', 'ConversationAPIController@deleteOfGroup')->middleware("auth:customers");

Route::resource('messages', 'MessageAPIController')->middleware("auth:customers");

Route::post('messages/send/group', 'MessageAPIController@store')->middleware("auth:customers");

Route::post('customer_devices', 'CustomerDeviceAPIController@store')->middleware("auth:customers");
