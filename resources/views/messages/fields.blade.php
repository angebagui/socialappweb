<!-- Content Field -->
<div class="form-group col-sm-6">
    {!! Form::label('content', 'Content:') !!}
    {!! Form::text('content', null, ['class' => 'form-control']) !!}
</div>

<!-- Sender Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sender_id', 'Sender Id:') !!}
    {!! Form::text('sender_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Conversation Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('conversation_id', 'Conversation Id:') !!}
    {!! Form::text('conversation_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Read Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_read', 'Is Read:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_read', 0) !!}
        {!! Form::checkbox('is_read', '1', null) !!} 1
    </label>
</div>

<!-- Is Received Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_received', 'Is Received:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_received', 0) !!}
        {!! Form::checkbox('is_received', '1', null) !!} 1
    </label>
</div>

<!-- Is Sent Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_sent', 'Is Sent:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_sent', 0) !!}
        {!! Form::checkbox('is_sent', '1', null) !!} 1
    </label>
</div>

<!-- Content Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('content_type', 'Content Type:') !!}
    {!! Form::text('content_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('messages.index') !!}" class="btn btn-default">Cancel</a>
</div>
